extends Area2D

#velocidade deste objeto
var vel = 400
#ponto de partida
var dir = Vector2() setget set_dir
#distancia maxima deste objeto
var dist = 150
#dano deste objeto
var damage = 1

onready var initPos = self.global_position 

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	#faz o objeto se movimentar do eixo Vector2 a X velocidade pixel(delta* vel)
	translate(dir * vel * delta)
	#verifica se esta a mais de "dis" em relação ao ponto de partida 
	if global_position.distance_to(initPos) > dist:
		autodestroy()
		
#quando entra em uma area
func _on_mg_bullet_area_entered(area):
	#verifica se metodo "hit" existe
	if area.has_method("hit"):
		#ativa o metodo que esta no objeto da area
		area.hit(damage, self)
		
#setget
func set_dir(parameter):
	dir = parameter
		
func autodestroy():
	queue_free()
#quando colidir com corpo
#func _on_mg_bullet_body_entered(body):
#	queue_free()
