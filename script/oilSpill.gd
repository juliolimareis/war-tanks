extends Node2D

func _ready():
	pass
#dispara quando o corṕpo entrar neste objeto
func _on_oilSpill_body_entered(body):
	add_to_group("oil - "+str(body))

#ativa quando corpo sair de dentro deste objeto
func _on_oilSpill_body_exited(body):
	remove_from_group("oil - "+str(body))
