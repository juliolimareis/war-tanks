extends RigidBody2D
#força do ricochete
export var boncing = 0.3
#contrutor
func _ready():
	randomize()
	bounce = boncing
	#gravidade
	gravity_scale = 0
	#desaceleração do impulso
	linear_damp = 1
	#girar os fragmentos aleatoriamente de 100 * 20
	angular_velocity = randf() * 10
	#calculo da randomizaçao da posição da peças
	var dir = randf() * (PI * 2)
	#aplica impulso de acorso com parametros
	apply_impulse(Vector2() , Vector2(cos(dir), sin(dir)) * 200)
	$Polygon2D.scale = get_parent().scale
	$Polygon2D2.scale = get_parent().scale
	#connect("sleeping_state_changed",self,"_on_fragment_sleeping_state_changed")

#func _on_fragment_sleeping_state_changed():
#	print("sleeping!")
#	if sleeping:
#		var t = get_tree().create_timer(randf() * 4 + 2)
#		#espera oa tempo acabar para prosseguir
#		yield(t,"timeout")
#		queue_free()
