extends StaticBody2D
#carrega o objeto de fragmanto que sera explodido
const PRE_FRAG = preload("res://object/fragments/create_box_fragments.tscn")
#construtor
func _ready():
	#instancia a conexção de signal "hitted"
	$area_hit.connect("hitted", self, "on_area_hitted")
	#instancia a conexção de signal "destroid"
	$area_hit.connect("destroid", self, "on_area_destroid")
#função de sinal, chamada pós o sinal "hitted" em -> destroied.gd 
func on_area_hitted(damage, health, node):
	if damage > 5:
		#ativa a animação "big_shoot"
		$anim.play("big_shoot")
	else:
		#ativa a animação "smal_hit"
		$anim.play("smal_hit")
#função de sinal, chamada pós o sinal "destroid" em -> destroied.gd 
func on_area_destroid():
	#instancia o objeto create_box_fragments.tscn
	var frag = PRE_FRAG.instance()
	#define a posição de acordo com a posição desse objeto
	frag.global_position = global_position
	get_parent().add_child(frag)
	#destroi este objeto
	queue_free()
