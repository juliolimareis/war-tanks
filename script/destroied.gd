extends Area2D

#cria um sinal
signal hitted(damage, health, node)
signal destroid()

#vida deste objeto
export var health = 30

#função de dano
func hit(damage, node):
	health -= damage
	#emite para o sinal "hitted"
	emit_signal("hitted",damage, health, node)
	if health <= 0:
		#emite para o sinal "destroid"
		emit_signal("destroid")
		#destroi o parente, que posteriormente se destroi