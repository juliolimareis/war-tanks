tool
extends Node2D

#Obs: Usar update() no objeto quando usar _draw
func _draw():
	#cor da mira
	var color = Color(1,0,0)
	
	if get_parent().get_parent().loaded:
		#desenha um circulo
		color = Color(0,1,0)
	draw_circle(Vector2(0,0), 4, color)
