#inicio configuração de edição via Engine
tool
extends KinematicBody2D

#const
const SIZE_SHOOT = 10
#PI = 180 graus
const ROT_VEL = PI/6
#velocidade do tank
const MAX_SPEED = 100

#desaceleração do tank
var vel_mod = 1
#resultado de movimentação
var travel = 0

var loaded = true

#aceleração
var acell = 0
#pré instancia objeto bala
var pre_bullet    = preload("res://object/Bullet.tscn")
var pre_track     = preload("res://object/Track.tscn")
var pre_mg_bullet = preload("res://object/mg_bullet.tscn")
# set_get -> configura set e get para a variável (body)
export(int, "bigRed","blue","dark","darkLarge","green","huge","red","sand") var body = 4 setget set_body
export(int, "special","tankBlue","tankDark","tankGreen","tankRed","tankSand") var barrel = 3 setget set_barrel

#endereço da imagem do corpo
var bodies = [
	"res://img/sprites/tankBody_bigRed.png",
	"res://img/sprites/tankBody_blue.png",
	"res://img/sprites/tankBody_dark.png",
	"res://img/sprites/tankBody_darkLarge.png",
	"res://img/sprites/tankBody_green.png",
	"res://img/sprites/tankBody_huge.png",
	"res://img/sprites/tankBody_red.png",
	"res://img/sprites/tankBody_sand.png"	
]
#endereço das imagens do cano
var barrels = [
	"res://img/sprites/specialBarrel5_outline.png",
	"res://img/sprites/tankBlue_barrel1_outline.png",
	"res://img/sprites/tankDark_barrel1_outline.png",
	"res://img/sprites/tankGreen_barrel1_outline.png",
	"res://img/sprites/tankRed_barrel1_outline.png",
	"res://img/sprites/tankSand_barrel1_outline.png"
]

#onready -> inicializará como construtor, pós que objeto for iniciado 
onready var selfGroupBullet = "bullet - "+str(self)
# construtor
func _ready():
	pass
	
#redesenha o objeto redesenhar 
func _draw():
	get_node("corpo").texture = load(bodies[body])
	get_node("barrel/barr").texture = load(barrels[barrel])
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	#verifica se esta no modo editor
	if Engine.editor_hint:
		return
	vel_mod = 1
	#desacelera o tank
	if get_tree().get_nodes_in_group("oil - "+str(self)).size() > 0:
		vel_mod = 0.3
	physicsTank(delta)
	#inicia as funões do tank
	#moveTank(delta)
	shoot()

############## = FUNÇÕES = #############

func physicsTank(delta):
	#valor adicionar da rotação
	var rot = 0
	var dir = 0
	
	if Input.is_action_pressed("ui_right"):
		rot += 1
		
	if Input.is_action_pressed("ui_left"):
		rot -= 1
		
	if Input.is_action_pressed("ui_up"):
		dir += 1
		
	if Input.is_action_pressed("ui_down"):
		dir -= 1
	#controle de rotação do corpo
	rotate(ROT_VEL * rot * delta )	
	#faz o sistema de acelraçao, progredindo os valores
	acell = lerp(acell, MAX_SPEED * dir, .01)
	#move o tank e salva valor da movimentação
	var move = move_and_slide( Vector2(cos(rotation), sin(rotation)) * acell * vel_mod)
	#soma o valor do da rota em pixels por segundo (* delta)
	travel += move.length() * delta
	
	#verifica se a rota foi maior que o comprimento do corpo da tank(eixo Y)
	if travel > $corpo.texture.get_size().y:
		#reseta o valor da rota
		travel = 0
		#instancia a esteira
		var track = pre_track.instance()
		#cria a esteria e adiciona a posição igual a posição tank menos(-) o valor do angulo * 5
		track.global_position = global_position - Vector2(cos(rotation), sin(rotation)) * 5
		#adiciona o angulo na esteira igual a do tank
		track.rotation = rotation
		#altera o valor index da esteira para o valor do z-index do tank-1
		track.z_index = (z_index - 1)
		#adiciona o objeto no nó pai
		$"../".add_child(track)
	
	#move cano do tank na direção do mouse	
	$barrel.look_at(get_global_mouse_position())
#cria a bala e atira 
func shoot():
	if loaded == false :
		return
	#se evento ui_shoot(left_mouse,space,ctr) for acionado
	if Input.is_action_just_pressed("ui_shoot"):
		#verifica quantos objestos tem no grupo
#		if get_tree().get_nodes_in_group(selfGroupBullet).size() < SIZE_SHOOT:
		#instancia uma bala
		var bullet = pre_bullet.instance()
		bullet.add_to_group(selfGroupBullet)
		#envia o seno e corseno para a variável de direção no objeto bala
		bullet.dir = Vector2( cos($barrel.global_rotation) , sin($barrel.global_rotation) ).normalized()
		#adiciona a bala na mesma posição so objeto "muzzle"
		bullet.global_position = $"barrel/muzzle".global_position
		#adiciona a bala no objeto pai
		#$"../".add_child(bullet) <- ou -> get_parent().add_child(bullet)
		get_parent().add_child(bullet)
		#ativa animação do tiro ( $"barrel/AnimationPlayer".play("fire") ) 
		get_node("barrel/AnimationPlayer").play("fire")
		loaded = false
		$barrel/sight.update()
		$reload.start()
		$barrel/shoot.play("shoot")
		
	if Input.is_action_just_pressed("machine_gun"):
		$mg.start()
		
	if Input.is_action_just_released("machine_gun"):
		$mg.stop()
		
#faz a movimentação do tank
func moveTank(delta):
	var dir_x = 0
	var dir_y = 0

	if Input.is_action_pressed("ui_right"):
		dir_x += 1
		
	if Input.is_action_pressed("ui_left"):
		dir_x -= 1
		
	if Input.is_action_pressed("ui_up"):
		dir_y -=1
		
	if Input.is_action_pressed("ui_down"):
		dir_y +=1
	#gira o tanque de acordo com mouse
	look_at(get_global_mouse_position())
	#move o tank para a direção x e y na velocidade XX
	move_and_slide( Vector2(dir_x,dir_y) * MAX_SPEED )
		
#setget
func set_body(val):
	body = val
	#verifica se esta do modo esitor
	if Engine.editor_hint:
		#atualiza o script via função _draw()
		update()
		
#setget
func set_barrel(val):
	barrel = val
	#verifica se esta do modo esitor
	if Engine.editor_hint:
		#atualiza o script via função _draw()
		update()
		
#dispara depois de 1 segundo
func _on_reload_timeout():
	loaded = true
	$barrel/sight.update()
		
func shoot_mg():
	var mg = pre_mg_bullet.instance()
	mg.dir = Vector2( cos($barrel.global_rotation) , sin($barrel.global_rotation) ).normalized()
	mg.global_position = $barrel/mg_muzzle.global_position
	get_parent().add_child(mg)
		
#ativa uma vez por segundo
func _on_mg_timeout():
	shoot_mg()
