extends Area2D

#distancia maxima da bala
var max_dist = 1000
#valor do nado deste objeto
var damage = 10

#velocidade da bala
var speed = 400
#vetor X, Y direção da bala. setget -> chama a função "set_dir" quando dir receber valor
var dir = Vector2(0,-1) setget set_dir
#para saber se a bala esta viva
var live = true

#recebe a posição inicial deste objeto
onready var startPos = global_position
#construtor
func _ready():
	pass

#inicia o script. delta -> frames por segundo 
func _process(delta):
	if live:
		#verifica se tem MAX_DIST ou mais pixels em relação a posição inicial
		if global_position.distance_to(startPos) >= max_dist:
			selfDestruction()
		#função de movimento, vetor de direção X,Y na velocidade XX
		translate(dir * delta* speed)

######### = FUNÇÕES = ###########
#função de destruição do proprio objeto
func selfDestruction():
#cancela o trabalho da particula
	$smoke.emitting = false
	live = false
	#esconde a bala
	$bullet.visible = false
	#ativa animação explode
	$animeSelfDestruction.play("explode")
	#desliga as ações do objeto 
	#Obs:deve ser usado antes do yield
	call_deferred("set_monitoring",false)
	call_deferred("set_monitorable", false)
	#prioridade de execução, até que signals -> "animation_finished"
	yield($animeSelfDestruction,"animation_finished")
	#destroi o objeto
	queue_free()
		
#verifica se objeto saiu da tela
func _on_VisibilityNotifier2D_screen_exited():
	#apaga este objeto
	queue_free()
		
#setget 
func set_dir(val):
	dir = val
	rotation = dir.angle()
		
#quando um corpo entrar
func _on_Bullet_body_entered(body):
	#verifica se leyer da colisão é do tipo "wall"(4)
	if body.collision_layer == 4:
		#destroi este objeto
		selfDestruction()
		
#quando entrar em uma area
func _on_Bullet_area_entered(area): 
	if area.has_method("hit"):
		area.hit(damage, self)
